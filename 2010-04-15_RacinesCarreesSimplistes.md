Title: Racines carrées Simplistes
Date: 2010-04-15
Tags: LaTeX
Category: Blog

![LaTeX](http://upload.wikimedia.org/wikipedia/commons/thumb/2/22/LaTeX_cover.svg/400px-LaTeX_cover.svg.png)

Extraire des racines carrées de nombres ≥1 en utilisant TEX et seulement avec des variables de type entier...

L'algorithme est catastrophique mais le fonctionnement des boucles y est instructif.


    \documentclass[12pt,a4paper]{article}
    
    \begin{document}
    Le dernier exemple ne fonctionne pas \par
    \newcount\N \N=1
    \loop\ifnum\N<10
      \the\N~Je dois relire le {\TeX}Book \par \advance\N by1
      \repeat
      
      \newcommand{\racine}[1]{
      \newcount\a \a=#1 \multiply\a by1000000
      \newcount\x \x=0
      \newcount\xx \xx=\x
      \loop\ifnum\xx<\a \advance\x by1 \xx=\x \multiply\xx by\x \repeat
      \newcount\intRac \intRac=\x \divide\intRac by1000-
      \newcount\iR \iR=\intRac \multiply\iR by1000
      \newcount\fracRac \fracRac=\x \advance\fracRac by-\iR
      \the\intRac.\the\fracRac}
      
      $\sqrt{8}\simeq\racine{8}$\\ 
      $\sqrt{18}\simeq\racine{18}$\\
      $\sqrt{100}=\racine{100}$\\
      $\sqrt{0.48}\simeq\racine{100}$
      
      \end{document}
